// On récupère les éléments

const canvas = document.getElementById('canvas');
const score = document.getElementById('score');
const days = document.getElementById('days');
const endScreen = document.getElementById('endScreen');
const musique = document.getElementById('musique');
const bruit = document.getElementById('bruit');
const perdu = document.getElementById('perdu');
const victoire = document.getElementById('victoire');



daysLeft = 31;
gameOverNumber = 40;
loopPlay = false;


// afficher le compteur score
function start(){
    count = 0
    getFaster = 6000;
    daysRemaining = daysLeft;
    canvas.innerHTML = '';
    score.innerHTML = count;
    days.innerHTML = daysRemaining;
    // musique de fond
    musique.play();
    bruit.onended = function(){
        bruit.pause();
        bruit.currentTime = 0;
    }

    
    
    

 //Affiche plusieurs virus

 // Assurer de ne pas jouer en bouce plusieurs fois 
    loopPlay ? '': game();
    loopPlay = true;

   function game() {

    let randomTime = Math.round(Math.random() * getFaster);
    getFaster > 700 ? getFaster = (getFaster * 0.90) : '';
        setTimeout(() => {
            if (daysRemaining === 0){
                youWin();
            } else if (canvas.childElementCount < gameOverNumber){ 
            virusPop();
            game();
            } else {
                gameOver();
            }
        }, randomTime);
   } 
// afficher la page du perdant avec la musique
   const gameOver = () => {
      endScreen.innerHTML = `<div class="gameOver">Game over <br/> le monde reste dans le chaos <br/>score : ${count}</div>`; 
      endScreen.style.visibility = 'visible';
      endScreen.style.opacity = '1';
      loopPlay = false;
      musique.pause();
      musique.currentTime = 0;
      perdu.play();
   };
// afficher la page du gagnant avec la musique
   const youWin = () => {
      let accuracy = Math.round(count / daysLeft * 100);
      endScreen.innerHTML = `<div class="youWin"> Félicitations ! Grâce à toi , thomas pourra payer la tournée à tout le monde dans les bars<br/><span>précision : ${accuracy}%</span></div>`; 
      endScreen.style.visibility = 'visible';
      endScreen.style.opacity = '1';
      loopPlay = false;
      musique.pause();
      musique.currentTime = 0;
      victoire.play();
   };

};
// Afficher les virus
function virusPop() {
    let virus = new Image();
    let virusg = new Image();
    let virusy = new Image();
    virus.src = "./photo/virus1.png";
    virusg.src = "./photo/virus2.png";
    virusy.src = "./photo/virus3.png";
    

    virus.classList.add('virus');
    virusg.classList.add('virus');
    virusy.classList.add('virus');

    virus.style.top = Math.random() * 250 + 'px';
    virusg.style.top = Math.random() * 250 + 'px';
    virusy.style.top = Math.random() * 250 + 'px';

    virus.style.left = Math.random() * 250 + 'px';
    virusg.style.left = Math.random() * 250 + 'px';
    virusy.style.left = Math.random() * 250 + 'px';

    let x, y;
    x = y = (Math.random() * 45) + 30;
    virus.style.setProperty('--x', `${ x }px`);
    virusg.style.setProperty('--x', `${ x }px`);
    virusy.style.setProperty('--x', `${ x }px`);

    virus.style.setProperty('--y', `${ y }px`);
    virusg.style.setProperty('--y', `${ y }px`);
    virusy.style.setProperty('--y', `${ y }px`);
    // définir son mouvement
    let plusMinus = Math.random() < 0.5 ? -1 : 1;
    let trX = Math.random() * 250 * plusMinus;
    let trY = Math.random() * 250 * plusMinus;
    virus.style.setProperty('--trX', `${ trX }%`);
    virusg.style.setProperty('--trX', `${ trX }%`);
    virusy.style.setProperty('--trX', `${ trX }%`);

    virus.style.setProperty('--trY', `${ trY }%`);
    virusg.style.setProperty('--trY', `${ trY }%`);
    virusy.style.setProperty('--trY', `${ trY }%`);

    // On précise que le virus est l'enfant du canvas
    canvas.appendChild(virus);
    canvas.appendChild(virusg);
    canvas.appendChild(virusy);
}

// Supprimer les éléments en cliquant
document.addEventListener('click', function(e){
    let targetElement = e.target || e.srcElement;
    if (targetElement.classList.contains('virus')){
        targetElement.remove();
        count++;
        score.innerHTML = count;
        bruit.play();
    };
});

//compte à rebours pour chaque clic
canvas.addEventListener('click', () => {
    if (daysRemaining > 0) {
        daysRemaining--;
        days.innerHTML = daysRemaining;
    }
});

// Masquer l'écran avec le clic
endScreen.addEventListener('click', () => {
    setTimeout(() => {
        start();
        endScreen.style.opacity = '0';
        endScreen.style.visibility = 'hidden';
    }, 3500)
   
});













